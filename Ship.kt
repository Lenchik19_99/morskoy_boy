package uz.SherAlex.www

class Ship(var size: Int) {
    var status: String = "Alive"
    var deadCells = 0
    var cells = arrayListOf<Cell>()

    fun make(cell: Cell, dir: String): Boolean {
        when (dir) {
            SHIP_DIRECTION_NORTH -> for (i in 0 until size) {
                if (cell.y + i > 10) {
                    return false
                }
                cells.add(Cell(cell.x, cell.y + i))
            }
            SHIP_DIRECTION_SOUTH -> for (i in 0 until size) {
                if (cell.y - i < 1) {
                    return false
                }
                cells.add(Cell(cell.x, cell.y - i))
            }
            SHIP_DIRECTION_WEST -> for (i in 0 until size) {
                if (cell.x - i < 1) {
                    return false
                }
                cells.add(Cell(cell.x - i, cell.y))
            }
            SHIP_DIRECTION_EAST -> for (i in 0 until size) {
                if (cell.x + i > 10) {
                    return false
                }
                cells.add(Cell(cell.x + i, cell.y))
            }
            else -> println("Найди свой жизненный путь, парниша")
        }
        return true
    }

    fun fire(cell: Cell): String {
        var cellReturnStatus = SHIP_STATUS_ALIVE
        for (ourCell in this.cells) {
            if (cell.x == ourCell.x && cell.y == ourCell.y) {
                ourCell.status = CELL_STATUS_FIRED
                this.status = SHIP_STATUS_FIRED
                cellReturnStatus = SHIP_STATUS_FIRED
                this.deadCells++
            }
        }
        if (this.deadCells == this.size) {
            this.status = SHIP_STATUS_DEAD
            if (cellReturnStatus == SHIP_STATUS_FIRED) {
                cellReturnStatus = SHIP_STATUS_DEAD
            }
        }

        return cellReturnStatus
    }
}