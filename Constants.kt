package uz.SherAlex.www

const val  CELL_STATUS_EMPTY = "Empty"
const val  CELL_STATUS_MISSED = "Missed"
const val  CELL_STATUS_FIRED = "Fired"

const val  SHIP_DIRECTION_NORTH = "N"
const val  SHIP_DIRECTION_EAST = "E"
const val  SHIP_DIRECTION_WEST = "W"
const val  SHIP_DIRECTION_SOUTH = "S"

const val SHIP_STATUS_ALIVE = "Alive"
const val SHIP_STATUS_FIRED = "Fired"
const val SHIP_STATUS_DEAD = "Dead"