package battleship

class Board {
    private val sizeBoard = 10
    var cells = arrayListOf<Cell>()
    var ships = arrayListOf<Ship>()

    constructor(sizeBoard: Int) {
        init(this.sizeBoard)
    }

    fun check(ourShip: Ship): Boolean {
        ships.forEach { ship ->
            ship.cells.forEach { shipCell ->
                (-1..1).forEach { i ->
                    (-1..1).forEach { j ->
                        ourShip.cells.forEach { ourCell ->
                            if (ourCell.x + i == shipCell.x && ourCell.y + j == shipCell.y) return false
                        }
                    }
                }
            }
        }
        return true
    }

    private fun init(sizeBoard: Int) {
        for (y in 1..sizeBoard) for (x in 1..sizeBoard) cells.add(Cell(x, y))
    }

    fun addShip(size: Int, cell: Cell, dir: String): Boolean {
        var newShip = Ship(size)
        return if (newShip.make(cell, dir)) {
            if (this.check(newShip)) {
                ships.add(newShip)
                true
            } else false
        } else false
    }

    fun check(cell: Cell): String = cell.status
    fun fire(cell: Cell): Cell {
        cell.status = CELL_STATUS_MISSED
        for (ship in ships) {
            val statusOfFire = ship.fire(cell)
            if (statusOfFire == SHIP_STATUS_FIRED
                || statusOfFire == SHIP_STATUS_DEAD
            ) {
                cell.status = statusOfFire
            }
        }
        return cell
    }
}